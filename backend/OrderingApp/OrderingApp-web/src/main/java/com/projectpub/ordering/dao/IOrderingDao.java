/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.dao;

import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.OrderState;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductOrder;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.NonNull;

/**
 *
 * @author Bob Steers
 */
public interface IOrderingDao {

    ProductOrder addOrder(@NonNull ProductOrder order);

    ProductOrder update(@NonNull ProductOrder order);

    Optional<ProductOrder> findNextOrder(
            OrderState state,
            OrderState newState,
            Set<ProductCategory> categories);

    List<ProductOrder> findOrders(OrderState state);

    void abort(@NonNull ProductOrder order);

    long queuedProductCount(@NonNull Product product, OrderState state);
}
