/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.rest;

import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductOrder;
import com.projectpub.ordering.services.IOrderingService;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author Bob Steers
 */
@Log4j
@Path("ordering")
@Stateless
@PermitAll
public class OrderingAPI {

    @Inject
    private IOrderingService service;

    @Path("create")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductOrder createOrder() {
        ProductOrder newOrder = service.create();
        return newOrder;
    }

    @Path("update")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductOrder updateOrder(ProductOrder order) {
        ProductOrder updatedOrder = service.update(order);
        return updatedOrder;
    }

    @Path("fulfill")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductOrder fulfillOrder(ProductOrder order) {
        ProductOrder updatedOrder = service.fulfill(order);
        return updatedOrder;
    }

    @Path("trymake")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductOrder tryMakeOrder() {
        Optional<ProductOrder> makeable = service.tryMake();
        return makeable.orElse(null);
    }

    @Path("categorytrymake")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductOrder tryMakeCategoryOrder(List<ProductCategory> categories) {
        Optional<ProductOrder> makeable = service.tryMake(new HashSet<>(categories));
        return makeable.orElse(null);
    }

    @Path("finishmake")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductOrder finishMakeOrder(ProductOrder order) {
        ProductOrder updatedOrder = service.finishMake(order);
        return updatedOrder;
    }

    @Path("trydeliver")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductOrder tryDeliverOrder() {
        Optional<ProductOrder> order = service.tryDeliver();
        return order.orElse(null);
    }

    @Path("finishdeliver")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductOrder finishDeliverOrder(ProductOrder order) {
        ProductOrder updatedOrder = service.finishDeliver(order);
        return updatedOrder;
    }

    @Path("queuecount")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public String queuedProductCount(Product product) {
        Long count = service.queuedProductCount(product);
        return count.toString();
    }

    @Path("alldelivered")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<ProductOrder> getDelivered() {
        List<ProductOrder> delivered = service.getDelivered();
        // Keeps behaviour in line with the rest of the API
        // On no result, should return 204 no content, and not an empty array
        return delivered.isEmpty() ? null : delivered;
    }

    @Path("pay")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void processPayment(ProductOrder order) {
        service.processPayment(order);
    }

}
