/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services;

import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.ProductOrder;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 *
 * @author Bob Steers
 */
public interface IOrderingService {

    ProductOrder create();

    ProductOrder update(ProductOrder order);

    ProductOrder fulfill(ProductOrder order);

    Optional<ProductOrder> tryMake();

    Optional<ProductOrder> tryMake(Set<ProductCategory> categories);

    ProductOrder finishMake(ProductOrder order);

    Optional<ProductOrder> tryDeliver();

    ProductOrder finishDeliver(ProductOrder order);

    Long queuedProductCount(Product product);

    List<ProductOrder> getDelivered();

    void processPayment(ProductOrder order);
}
