/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services;

import com.projectpub.ordering.entities.Product;
import java.util.List;

/**
 *
 * @author Bob Steers
 */
public interface IProductService {

    public List<Product> getAllProducts();

    public List<Product> reserve(List<Product> products);

    public List<Product> use(List<Product> products);
}
