/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.resources;

import com.projectpub.ordering.qualifiers.DevBean;
import com.projectpub.ordering.qualifiers.LiveBean;
import com.projectpub.ordering.qualifiers.TestBean;
import com.projectpub.ordering.services.IProductService;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 *
 * @author Bob Steers
 */
@Stateless
public class ProductServiceFactory {

    @Inject
    @DevBean
    private IProductService devService;

    @Inject
    @LiveBean
    private IProductService liveService;

    @Inject
    @TestBean
    private IProductService testService;

    @Produces
    public IProductService produceProductService() {

        String pubEnv = System.getenv("PROJECT_PUB_ENV");

        switch (pubEnv) {
            case "live":
                return liveService;
            case "test":
                return testService;
            case "dev":
                return devService;
            default:
                throw new IllegalArgumentException("Unknown Project Pub environment: " + pubEnv);
        }
    }
}
