/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import com.projectpub.ordering.qualifiers.FrontendTarget;
import com.projectpub.ordering.services.IPushNotificationService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.Getter;

/**
 *
 * @author Bob Steers
 */
@ApplicationScoped
@FrontendTarget
public class FrontendNotificationServiceImpl extends OrderNotificationServiceImpl{
    
    @Inject
    @Getter
    @FrontendTarget
    IPushNotificationService pushService;
}
