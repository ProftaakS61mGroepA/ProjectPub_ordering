/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.entities.listeners;

import com.projectpub.ordering.entities.ProductOrder;
import com.projectpub.ordering.qualifiers.BackendTarget;
import com.projectpub.ordering.services.IOrderNotificationService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PostUpdate;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author Bob Steers
 */
@Log4j
@Stateless
public class ProductOrderListener {

    @Inject
    @BackendTarget
    private IOrderNotificationService service;

    @PostUpdate
    void onPostUpdate(ProductOrder order) {
        service.notifyOrigin(order);

        switch (order.getOrderState()) {
            case CREATED:
            case MAKEABLE:
            case MAKING:
            case DELIVERABLE:
            case DELIVERING:
                break; // not handled
            case DELIVERED:
                service.notifyDelivered(order);
                break;
            case COMPLETED: // fall-through
            case ABORTED:
                service.notifyCompleted(order);
                break;
            default:
                throw new AssertionError(order.getOrderState().name());

        }
    }
}
