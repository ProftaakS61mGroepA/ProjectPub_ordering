/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.dao;

import com.mysema.query.jpa.EclipseLinkTemplates;
import com.mysema.query.jpa.impl.JPAQuery;
import com.projectpub.ordering.entities.OrderState;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.ProductOrder;
import com.projectpub.ordering.entities.QProduct;
import com.projectpub.ordering.entities.QProductOrder;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;

@Stateless
@Log4j
public class OrderingPersistenceDao implements IOrderingDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public ProductOrder addOrder(ProductOrder order) {
        return update(order);
    }

    @Override
    public ProductOrder update(ProductOrder order) {
        ProductOrder merged = em.merge(order);
        return merged;
    }

    @Override
    @Transactional
    public Optional<ProductOrder> findNextOrder(
            OrderState state,
            OrderState newState,
            Set<ProductCategory> categories) {
        QProductOrder orders = QProductOrder.productOrder;

        Optional<ProductOrder> order = Optional.ofNullable(
                new JPAQuery(em, EclipseLinkTemplates.DEFAULT)
                        .from(orders)
                        .where(orders.orderState.eq(state))
                        .orderBy(orders.id.asc())
                        .limit(1)
                        .singleResult(orders));

        order.ifPresent((val) -> {
            val.setOrderState(newState);
            update(val);
        });
        return order;
    }

    @Override
    public void abort(ProductOrder order) {
        em.remove(order);
    }

    @Override
    public long queuedProductCount(@NonNull Product product, OrderState state) {
        QProductOrder qOrder = QProductOrder.productOrder;
        QProduct qProduct = QProduct.product;
        long count = new JPAQuery(em, EclipseLinkTemplates.DEFAULT)
                .from(qOrder)
                .innerJoin(qOrder.products, qProduct)
                .where(qOrder.orderState.eq(state))
                .where(qProduct.id.eq(product.getId()))
                .count();

        return count;
    }

    @Override
    public List<ProductOrder> findOrders(OrderState state) {
        QProductOrder qOrder = QProductOrder.productOrder;
        QProduct qProduct = QProduct.product;

        List<ProductOrder> results
                = new JPAQuery(em, EclipseLinkTemplates.DEFAULT)
                        .from(qOrder)
                        .where(qOrder.orderState.eq(state))
                        .list(qOrder);

        return results;
    }

}
