/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.messaging;

import com.projectpub.ordering.services.IConfigService;
import java.util.Properties;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import lombok.extern.log4j.Log4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;

/**
 *
 * @author Bob Steers
 */
@Stateless
@Log4j
public class MessageResourceProducer {

    private String getBrokerName() {
        return System.getenv().get("KAFKA_BROKER");
    }

    @Produces
    public KafkaProducer<String, String> createProducer() {
        Properties kafkaProps = new Properties();

        kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, getBrokerName());
        kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put(ProducerConfig.ACKS_CONFIG, "0");

        log.info("created Kafka producer: " + kafkaProps);

        return new KafkaProducer<>(kafkaProps);
    }

    @Produces
    public KafkaConsumer<String, String> createConsumer() {
        Properties kafkaProps = new Properties();

        // Using a unique group name guarantees that all messages are published
        // to all subscribers
        String uniqueGroupName = UUID.randomUUID().toString();

        kafkaProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, getBrokerName());
        kafkaProps.put(ConsumerConfig.GROUP_ID_CONFIG, uniqueGroupName);
        kafkaProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        kafkaProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        log.info("created Kafka consumer: " + kafkaProps);

        return new KafkaConsumer<>(kafkaProps);
    }
}
