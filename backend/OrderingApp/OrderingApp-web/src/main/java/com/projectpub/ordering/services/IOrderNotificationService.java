/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services;

import com.projectpub.ordering.entities.ProductOrder;

/**
 *
 * @author Bob Steers
 */
public interface IOrderNotificationService {

    public void notifyOrigin(ProductOrder order);
    
    public void notifyDelivered(ProductOrder order);
    
    public void notifyCompleted(ProductOrder order);
}
