/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services;

/**
 *
 * @author Bob Steers
 */
public interface IConfigService {

    public String get(String key);
}
