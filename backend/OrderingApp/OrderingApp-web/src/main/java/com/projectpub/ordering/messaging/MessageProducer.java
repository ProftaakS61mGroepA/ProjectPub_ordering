/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectpub.ordering.qualifiers.BackendTarget;
import com.projectpub.ordering.services.IConfigService;
import com.projectpub.ordering.services.IPushNotificationService;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import lombok.extern.log4j.Log4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author Bob Steers
 */
@Log4j
@ApplicationScoped
@BackendTarget
@Default
public class MessageProducer implements IPushNotificationService {

    private String topicName;

    @Inject
    private KafkaProducer<String, String> kafkaProducer;
    
    @Inject
    private IConfigService config;
    
    @PostConstruct
    public void configure() {
        topicName = config.get("messaging.broker.topic");
    }
 
    @Override
    public void broadcastObject(Object message) throws IOException {
        String json = new ObjectMapper().writeValueAsString(message);
        broadcastJSON(json);
    }

    @Override
    public void broadcastJSON(String json) throws IOException {
        kafkaProducer.send(new ProducerRecord<>(topicName, null, json));
    }

}
