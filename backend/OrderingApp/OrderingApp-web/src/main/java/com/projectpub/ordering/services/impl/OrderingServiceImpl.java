/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import com.projectpub.ordering.dao.IOrderingDao;
import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.OrderState;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductOrder;
import com.projectpub.ordering.services.IOrderingService;
import com.projectpub.ordering.services.IProductService;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author Bob Steers
 */
@Log4j
@Stateless
public class OrderingServiceImpl implements IOrderingService {

    @Inject
    private IOrderingDao dao;

    @Inject
    private IProductService productService;

    private Set<ProductCategory> allCategories() {
        return new HashSet<>(Arrays.asList(ProductCategory.values()));
    }

    @Override
    public ProductOrder create() {
        ProductOrder created = dao.addOrder(new ProductOrder());
        return created;
    }

    @Override
    public ProductOrder update(@NonNull ProductOrder order) {
        ProductOrder updated = dao.update(order);
        return updated;
    }

    @Override
    public ProductOrder fulfill(@NonNull ProductOrder order) {
        if (order.getProducts().isEmpty()) {
            throw new IllegalArgumentException("Unable to fulfill empty order");
        }
        productService.reserve(order.getProducts());
        order.setOrderState(OrderState.MAKEABLE);
        ProductOrder updated = dao.update(order);
        return updated;
    }

    @Override
    public Optional<ProductOrder> tryMake() {
        Optional<ProductOrder> order = dao.findNextOrder(
                OrderState.MAKEABLE, OrderState.MAKING, allCategories());
        return order;
    }

    @Override
    public Optional<ProductOrder> tryMake(Set<ProductCategory> categories) {
        if (categories.isEmpty()) {
            return Optional.ofNullable(null);
        }

        Optional<ProductOrder> order = dao.findNextOrder(
                OrderState.MAKEABLE,
                OrderState.MAKING,
                categories);

        order.ifPresent((item) -> {
            Map<Boolean, List<Product>> partitionedMap
                    = item.getProducts()
                            .stream()
                            .collect(Collectors.partitioningBy(
                                    prod -> categories.contains(prod.getCategory())));

            if (!partitionedMap.get(false).isEmpty()) {
                // update current item to only contain wanted products
                item.setProducts(partitionedMap.get(true));

                // create new order for the unwanted products
                ProductOrder unwanted = new ProductOrder();
                unwanted.setOrderState(OrderState.MAKEABLE);
                unwanted.setProducts(partitionedMap.get(false));
                dao.addOrder(unwanted);
            }

            // use all wanted products
            productService.use(item.getProducts());
        });

        return order;
    }

    @Override
    public ProductOrder finishMake(@NonNull ProductOrder order) {
        if (order.getOrderState() != OrderState.MAKING) {
            throw new IllegalStateException("Order is not currently being made");
        }
        order.setOrderState(OrderState.DELIVERABLE);
        ProductOrder updated = dao.update(order);
        return updated;
    }

    @Override
    public Optional<ProductOrder> tryDeliver() {
        Optional<ProductOrder> order = dao.findNextOrder(
                OrderState.DELIVERABLE, OrderState.DELIVERING, allCategories());
        return order;
    }

    @Override
    public ProductOrder finishDeliver(@NonNull ProductOrder order) {
        if (order.getOrderState() != OrderState.DELIVERING) {
            throw new IllegalStateException("Order is not currently being delivered");
        }
        order.setOrderState(OrderState.DELIVERED);
        ProductOrder updated = dao.update(order);
        return updated;
    }

    @Override
    public Long queuedProductCount(Product product) {
        Long count = dao.queuedProductCount(product, OrderState.MAKEABLE);
        return count;
    }

    @Override
    public List<ProductOrder> getDelivered() {
        List<ProductOrder> orders = dao.findOrders(OrderState.DELIVERED);
        return orders;
    }

    @Override
    public void processPayment(ProductOrder order) {
        if (order.getOrderState() != OrderState.DELIVERED) {
            throw new IllegalStateException("Order was not yet delivered");
        }
        order.setOrderState(OrderState.COMPLETED);
        dao.update(order);
    }
}
