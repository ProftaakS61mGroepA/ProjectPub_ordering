/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.messaging;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;

/**
 *
 * @author Bob Steers
 */
@Singleton
@Startup
public class MessageListenerBootstrap {

    @Resource
    private ManagedExecutorService exec;
    
    @Inject
    private MessageListener listener;
    
    @PostConstruct
    public void bootstrap() {
        exec.submit(listener);
    }
}
