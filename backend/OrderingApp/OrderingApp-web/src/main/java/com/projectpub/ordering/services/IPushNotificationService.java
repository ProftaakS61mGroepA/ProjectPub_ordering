/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services;

import java.io.IOException;

/**
 *
 * @author Bob Steers
 */
public interface IPushNotificationService {

    void broadcastObject(Object message) throws IOException;

    void broadcastJSON(String json) throws IOException;
}
