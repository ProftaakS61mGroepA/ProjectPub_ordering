/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.entities;

import com.projectpub.ordering.entities.listeners.ProductOrderListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.inject.Model;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Bob Steers
 */
@NoArgsConstructor
@RequiredArgsConstructor
@ToString(of = "id")
@EqualsAndHashCode
@Model
@Entity
@EntityListeners({ProductOrderListener.class})
public class ProductOrder implements Serializable, Comparable<ProductOrder> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Getter
    @Setter
    @NonNull
    private long id;

    @Getter
    @Setter
    private OrderState orderState = OrderState.CREATED;

    @Getter
    @Setter
    private OrderOrigin origin = OrderOrigin.ORDERING;

    @Getter
    @Setter
    @ManyToMany
    private List<Product> products = new ArrayList<>();

    public void add(Product product) {
        products.add(product);
    }

    @Override
    public int compareTo(ProductOrder o) {
        return Long.compare(this.id, o.getId());
    }
}
