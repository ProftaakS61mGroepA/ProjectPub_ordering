/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import com.projectpub.ordering.entities.ProductOrder;
import com.projectpub.ordering.services.IConfigService;
import com.projectpub.ordering.services.IOrderNotificationService;
import com.projectpub.ordering.services.IPushNotificationService;
import javax.inject.Inject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.MediaType;

import lombok.NonNull;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;

@Log4j
public abstract class OrderNotificationServiceImpl implements IOrderNotificationService {

    @Inject
    IConfigService configService;

    protected abstract IPushNotificationService getPushService();

    private void asyncOrderStatusChangeRequest(String baseUrl, @NonNull ProductOrder order) {
        Client.create()
                .asyncResource(baseUrl + "/onorderstatuschange")
                .accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .put(ClientResponse.class, order);
    }

    @Override
    public void notifyOrigin(@NonNull ProductOrder order) {
        String url = configService.get("component."
                + order.getOrigin().toString().toLowerCase()
                + ".url");

        if (!StringUtils.isBlank(url)) {
            asyncOrderStatusChangeRequest(url, order);
        }
    }

    @Override
    public void notifyDelivered(ProductOrder order) {
        try {
            Map<String, ProductOrder> notification = new HashMap<>();
            notification.put("update", order);
            getPushService().broadcastObject(notification);
        } catch (IOException ex) {
            log.error("error broadcasting delivered order: ", ex);
        }
    }

    @Override
    public void notifyCompleted(ProductOrder order) {
        try {
            Map<String, ProductOrder> notification = new HashMap<>();
            notification.put("remove", order);
            getPushService().broadcastObject(notification);
        } catch (IOException ex) {
            log.error("error broadcasting completed order: ", ex);
        }
    }

}
