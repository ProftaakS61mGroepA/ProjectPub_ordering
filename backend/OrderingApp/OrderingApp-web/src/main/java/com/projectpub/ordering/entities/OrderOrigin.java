/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.entities;

/**
 *
 * @author Bob Steers
 */
public enum OrderOrigin {
    DRIVEIN,
    ORDERING
}
