/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.rest;

import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.services.IProductService;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j;
import org.glassfish.jersey.server.ManagedAsync;

/**
 *
 * @author Bob Steers
 */
@Log4j
@Path("products")
@Stateless
@PermitAll
public class ProductAPI {

    @Inject
    private IProductService service;

    @Path("all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ManagedAsync
    public void getAllProducts(@Suspended final AsyncResponse ar) {
        List<Product> allProducts = service.getAllProducts();
        ar.resume(allProducts);
    }

    @Path("categories")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ProductCategory[] getAllCategories() {
        return ProductCategory.values();
    }

}
