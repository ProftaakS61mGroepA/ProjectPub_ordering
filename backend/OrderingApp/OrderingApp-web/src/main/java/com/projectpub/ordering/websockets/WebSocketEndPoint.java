/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.websockets;

import java.text.MessageFormat;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author Bob Steers
 */
@ApplicationScoped
@ServerEndpoint("/delivered")
@Log4j
public class WebSocketEndPoint {

    @Inject
    private WebSocketSessionHandler sessionHandler;

    @OnOpen
    public void open(Session session) {
        sessionHandler.addSession(session);
    }

    @OnClose
    public void close(Session session) {
        sessionHandler.removeSession(session);
    }

    @OnError
    public void onError(Throwable error) {
    }

    @OnMessage
    public void handleMessage(String message, Session session) {
        // We're not handling incoming messages
        log.info(MessageFormat.format("[Session {0}] Message received: {1}",
                session.getId(),
                message));
    }

}
