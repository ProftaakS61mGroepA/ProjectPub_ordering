/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import com.projectpub.ordering.entities.Product;
import static com.projectpub.ordering.entities.ProductCategory.DRINK;
import static com.projectpub.ordering.entities.ProductCategory.FOOD;
import com.projectpub.ordering.qualifiers.DevBean;
import com.projectpub.ordering.qualifiers.TestBean;
import com.projectpub.ordering.services.IProductService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.Stateless;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author Bob Steers
 */
@Log4j
@Stateless
@DevBean
@TestBean
public class LocalProductServiceImpl implements IProductService {

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>(Arrays.asList(
                new Product(1, 1, "Coca Cola", 200, DRINK, 5),
                new Product(2, 2, "Trappist", 300, DRINK, 5),
                new Product(3, 3, "Tosti", 250, FOOD, 5)
        ));
        return products;
    }

    @Override
    public List<Product> reserve(@NonNull List<Product> products) {
        return products;
    }

    @Override
    public List<Product> use(@NonNull List<Product> products) {
        return products;
    }

}
