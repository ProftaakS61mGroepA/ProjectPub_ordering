/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.qualifiers.LiveBean;
import com.projectpub.ordering.services.IConfigService;
import com.projectpub.ordering.services.IProductService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import static java.net.HttpURLConnection.HTTP_OK;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author Bob Steers
 */
@Log4j
@Stateless
@LiveBean
public class RemoteProductServiceImpl implements IProductService {

    @Inject
    private IConfigService configService;

    private String stockUrl;
    
    @PostConstruct
    public void construct() {
        stockUrl = configService.get("component.stock.url");
    }

    @Override
    public List<Product> getAllProducts() {
        log.info("all products endpoint = " + stockUrl + "/products");
        List<Product> products = Client.create()
                .resource(stockUrl + "/products")
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Product>>() {
                });

        return products;
    }

    @Override
    public List<Product> reserve(@NonNull List<Product> products) {
        if (products.isEmpty()) {
            return products;
        }
        log.info("reserving products: " + products);
        ClientResponse response = Client.create()
                .resource(stockUrl + "/products/reserve/")
                .accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .put(ClientResponse.class, products);

        if (response.getStatus() != HTTP_OK) {
            throw new IllegalArgumentException(response.getStatusInfo().toString());
        }

        return response.getEntity(new GenericType<List<Product>>() {
        });
    }

    @Override
    public List<Product> use(@NonNull List<Product> products) {
        if (products.isEmpty()) {
            return products;
        }
        log.info("using products: " + products);
        ClientResponse response = Client.create()
                .resource(stockUrl + "/products/use/")
                .accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .put(ClientResponse.class, products);

        if (response.getStatus() != HTTP_OK) {
            throw new IllegalArgumentException(response.getStatusInfo().toString());
        }

        return response.getEntity(new GenericType<List<Product>>() {
        });
    }

}
