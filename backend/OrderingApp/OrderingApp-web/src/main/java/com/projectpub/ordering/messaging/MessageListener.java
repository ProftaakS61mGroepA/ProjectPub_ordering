/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.messaging;

import com.projectpub.ordering.qualifiers.FrontendTarget;
import com.projectpub.ordering.services.IConfigService;
import com.projectpub.ordering.services.IPushNotificationService;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import lombok.extern.log4j.Log4j;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;

/**
 *
 * @author Bob Steers
 */
@Dependent
@Log4j
public class MessageListener implements Runnable {

    @Inject
    @FrontendTarget
    IPushNotificationService service;

    @Inject
    KafkaConsumer<String, String> consumer;
    
    @Inject
    private IConfigService config;

    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    public void stop() {
        try {
            if (isRunning.getAndSet(false)) {
                consumer.wakeup();
            }
        } catch (Exception ex) {
            log.error("Error stopping listener", ex);
        }
    }

    @Override
    public void run() {
        try {
            
            consumer.subscribe(Arrays.asList(config.get("messaging.broker.topic")));
            log.info("Kafka consumer is running");
            
            while (isRunning.get()) {

                consumer.poll(Integer.MAX_VALUE).forEach(record -> {
                    try {
                        log.info("relaying message: " + record.value());
                        service.broadcastJSON(record.value());
                    } catch (IOException ex) {
                        log.error("Failed broadcasting JSON message", ex);
                    }
                });

            }
        } catch (WakeupException ex) {
            log.debug("Consumer loop interrupted from thread " + Thread.currentThread().getName());
        } catch (Exception ex) {
            log.error("MessageListener error", ex);
        } finally {
            consumer.close();
            log.info("Consumer shutdown");
        }
    }

}
