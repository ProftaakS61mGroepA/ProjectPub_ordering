/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.websockets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectpub.ordering.qualifiers.FrontendTarget;
import com.projectpub.ordering.services.IPushNotificationService;
import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.inject.Default;
import javax.websocket.Session;
import lombok.NonNull;
import lombok.Synchronized;
import lombok.extern.log4j.Log4j;

/**
 *
 * @author Bob Steers
 */
@ApplicationScoped
@FrontendTarget
@Default
@Log4j
public class WebSocketSessionHandler implements IPushNotificationService {

    private final Set<Session> sessions = new HashSet<>();

    @Synchronized("sessions")
    public void addSession(@NonNull Session session) {
        sessions.add(session);
    }

    @Synchronized("sessions")
    public void removeSession(@NonNull Session session) {
        try {
            if (session.isOpen()) {
                session.close();
            }
        } catch (IOException ex) {
            log.warn(ex);
        } finally {
            sessions.remove(session);
        }
    }

    @Synchronized("sessions")
    private Set<Session> snapshot() {
        return new HashSet<>(sessions);
    }

    @Override
    public void broadcastObject(@NonNull Object message) throws IOException {
        String json = new ObjectMapper().writeValueAsString(message);
        broadcastJSON(json);
    }

    @Override
    public void broadcastJSON(String json) throws IOException {
        log.info("broadcasting to " + sessions.size() + " WS sessions");
        snapshot().forEach(s -> send(s, json));
    }

    public void send(@NonNull Session session, @NonNull String message) {
        // We're scattershot broadcasting
        // Server doesn't care whether message is received
        session.getAsyncRemote().sendText(message);
    }

}
