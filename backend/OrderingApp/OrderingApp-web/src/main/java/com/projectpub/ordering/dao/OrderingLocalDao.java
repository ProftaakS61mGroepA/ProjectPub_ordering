/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.dao;

import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.OrderState;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.enterprise.inject.Alternative;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;

@Singleton
@Alternative
@Log4j
public class OrderingLocalDao implements IOrderingDao {

    private final Map<Long, ProductOrder> orders = new HashMap<>();

    private long nextId = 0;

    @Override
    public ProductOrder addOrder(ProductOrder order) {
        nextId++;
        order.setId(nextId);
        orders.put(nextId, order);
        return order;
    }

    @Override
    public ProductOrder update(ProductOrder order) {
        orders.replace(order.getId(), order);
        return order;
    }

    @Override
    public Optional<ProductOrder> findNextOrder(
            OrderState state,
            OrderState newState,
            Set<ProductCategory> categories) {
        Optional<ProductOrder> retVal = orders.values()
                .stream()
                .sorted()
                .filter(order -> order.getOrderState() == state)
                .peek((val) -> val.setOrderState(newState))
                .findFirst();
        retVal.ifPresent((val) -> update(val));
        return retVal;
    }

    @Override
    public void abort(ProductOrder order) {
        orders.remove(order.getId());
    }

    @Override
    public long queuedProductCount(@NonNull Product product, OrderState state) {
        return orders.values()
                .stream()
                .filter(order -> order.getOrderState() == state)
                .mapToLong(
                        order -> order.getProducts()
                                .parallelStream()
                                .filter(prod -> prod.equals(product))
                                .count()
                )
                .sum();
    }

    @Override
    public List<ProductOrder> findOrders(OrderState state) {
        return orders.values()
                .stream()
                .filter(order -> order.getOrderState() == state)
                .collect(Collectors.toList());
    }

}
