/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import com.projectpub.ordering.services.IConfigService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import lombok.extern.log4j.Log4j;

@Singleton
@Startup
@Log4j
public class ConfigServiceImpl implements IConfigService {

    Properties prop = new Properties();

    private File getFileFromURL(String path) {
        URL url = this.getClass().getClassLoader().getResource(path);
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        } finally {
            return file;
        }
    }

    @PostConstruct
    public void load() {
        String configFile
                = "config-" + System.getenv("PROJECT_PUB_ENV") + ".properties";
        log.info("configFile = " + configFile);
        try (
                InputStream defaultInput
                = new FileInputStream(getFileFromURL("config-default.properties"));
                InputStream input
                = new FileInputStream(getFileFromURL(configFile))) {

            // First load defaults, then let environment-specific overwrite
            prop.load(defaultInput);
            prop.load(input);
            
            log.info("config loaded");
            prop.entrySet()
                    .stream()
                    .forEach(entry -> log.info(entry.getKey() + " = " + entry.getValue()));
            
        } catch (IOException ex) {
            log.error(ex);
        }
    }

    @Override
    public String get(String key) {
        return prop.getProperty(key);
    }
}
