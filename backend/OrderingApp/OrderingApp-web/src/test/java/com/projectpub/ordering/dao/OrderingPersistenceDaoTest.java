/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.dao;

import com.projectpub.ordering.entities.ProductOrder;
import javax.persistence.EntityManager;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OrderingPersistenceDaoTest {

    @InjectMocks
    OrderingPersistenceDao dao;

    @Mock
    private EntityManager emMock;

    @Test
    public void testAddOrder() throws Exception {
        when(emMock.merge(any(ProductOrder.class)))
                .then(returnsFirstArg());
        
        ProductOrder order = new ProductOrder();
        assertEquals(order, dao.addOrder(order));
    }

    @Test
    public void testUpdate() throws Exception {
        when(emMock.merge(any(ProductOrder.class)))
                .then(returnsFirstArg());

        ProductOrder order = new ProductOrder(10);
        assertEquals(dao.update(order), order);
    }
}
