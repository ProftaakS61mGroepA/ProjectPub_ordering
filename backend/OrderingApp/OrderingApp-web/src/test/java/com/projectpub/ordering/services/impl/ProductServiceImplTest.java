/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import com.projectpub.ordering.services.impl.RemoteProductServiceImpl;
import com.projectpub.ordering.entities.Product;
import java.util.List;
import lombok.extern.log4j.Log4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bob Steers
 */
@Log4j
public class ProductServiceImplTest {

    private LocalProductServiceImpl service;

    @Before
    public void setUp() {
        service = new LocalProductServiceImpl();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetAllProducts() throws Exception {
        List<Product> products = service.getAllProducts();
        assertFalse(products.isEmpty());

        List<Product> repeat = service.getAllProducts();
        assertEquals(products, repeat);

        log.info("testprop = " + System.getProperty("testprop"));
    }

}
