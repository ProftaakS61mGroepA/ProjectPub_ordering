/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.rest;

import static com.projectpub.ordering.entities.ProductCategory.DRINK;
import static com.projectpub.ordering.entities.ProductCategory.FOOD;
import static org.junit.Assert.*;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductCategory;
import static com.projectpub.ordering.rest.APITestUtils.assertStatus;
import static com.projectpub.ordering.rest.APITestUtils.readCategoryList;
import static com.projectpub.ordering.rest.APITestUtils.readProductList;
import com.projectpub.ordering.services.IProductService;
import com.projectpub.ordering.services.impl.LocalProductServiceImpl;
import static java.net.HttpURLConnection.HTTP_OK;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Bob Steers
 */
@Log4j
@RunWith(MockitoJUnitRunner.class)
public class ProductAPITest extends JerseyTest {

    List<Product> dummies;

    @Mock
    private LocalProductServiceImpl service;

    @Override
    public Application configure() {
        ResourceConfig config = new ResourceConfig(ProductAPI.class);
        config.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(service).to(IProductService.class);
            }
        });
        return config;
    }

    private Response requestAllProducts() {
        return target("/products/all")
                .request(MediaType.APPLICATION_JSON)
                .get();
    }

    private Response requestAllCategories() {
        return target("/products/categories")
                .request(MediaType.APPLICATION_JSON)
                .get();
    }

    @Before
    public void setUpTest() {
        dummies = new ArrayList<>(Arrays.asList(
                new Product(1, 1, "Coca Cola", 200, DRINK, 5),
                new Product(2, 2, "Trappist", 300, DRINK, 5),
                new Product(3, 3, "Tosti", 250, FOOD, 5)
        ));
    }

    @After
    public void tearDownTest() {
    }

    @Test
    public void testGetAllProducts() throws Exception {
        when(service.getAllProducts()).thenReturn(dummies);

        Response response = requestAllProducts();
        assertStatus(response, HTTP_OK);
        assertEquals(dummies, readProductList(response));
    }

    @Test
    public void testGetAllCategories() throws Exception {
        Response response = requestAllCategories();
        assertStatus(response, HTTP_OK);
        assertEquals(
                Arrays.asList(ProductCategory.values()),
                readCategoryList(response));
    }

    @Test
    public void testGetAllCategoriesAsString() throws Exception {
        List<String> expectedVals = new ArrayList<>();
        for (ProductCategory cat : ProductCategory.values()) {
            expectedVals.add("\"" + cat.toString() + "\"");
        }
        String expected = "[" + String.join(",", expectedVals) + "]";

        Response response = requestAllCategories();
        assertStatus(response, HTTP_OK);

        String actual = response.readEntity(String.class);
        assertEquals(expected, actual);
    }

}
