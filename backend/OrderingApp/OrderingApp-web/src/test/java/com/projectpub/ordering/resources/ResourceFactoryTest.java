/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.resources;

import com.projectpub.ordering.services.impl.ConfigServiceImpl;
import com.projectpub.ordering.services.impl.LocalProductServiceImpl;
import com.projectpub.ordering.services.impl.RemoteProductServiceImpl;
import javax.inject.Inject;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

/**
 *
 * @author Bob Steers
 */
@RunWith(CdiRunner.class)
@AdditionalClasses({
    LocalProductServiceImpl.class,
    RemoteProductServiceImpl.class,
    ConfigServiceImpl.class})
public class ResourceFactoryTest {

    private final EnvironmentVariables environmentVariables
            = new EnvironmentVariables();

    @Rule
    public EnvironmentVariables getEnv() {
        // Workaround for the Cdirunner
        // If the rule is set directly on the member, weld will throw on "public member in bean"
        return environmentVariables;
    }

    @Inject
    ProductServiceFactory factory;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testProduceProductService() throws Exception {
        environmentVariables.set("PROJECT_PUB_ENV", "test");
        assertEquals(LocalProductServiceImpl.class, factory.produceProductService().getClass());

        environmentVariables.set("PROJECT_PUB_ENV", "dev");
        assertEquals(LocalProductServiceImpl.class, factory.produceProductService().getClass());

        environmentVariables.set("PROJECT_PUB_ENV", "live");
        assertEquals(RemoteProductServiceImpl.class, factory.produceProductService().getClass());
    }

}
