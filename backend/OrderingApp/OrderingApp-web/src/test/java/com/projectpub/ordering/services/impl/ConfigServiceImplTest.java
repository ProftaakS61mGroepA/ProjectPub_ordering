/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import lombok.extern.log4j.Log4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Bob Steers
 */
@Log4j
@RunWith(MockitoJUnitRunner.class)
public class ConfigServiceImplTest {

    @Rule
    public final EnvironmentVariables environmentVariables
            = new EnvironmentVariables();

    @InjectMocks
    private ConfigServiceImpl service;

    @Before
    public void setUp() {
        environmentVariables.set("PROJECT_PUB_ENV", "test");
        service.load();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLoad() throws Exception {
        assertEquals("test", System.getenv("PROJECT_PUB_ENV"));
    }

    @Test
    public void testGet() throws Exception {
        assertEquals("test_drivein", service.get("component.drivein.url"));
    }

}
