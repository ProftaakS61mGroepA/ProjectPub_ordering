/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.services.impl;

import static com.github.stefanbirkner.fishbowl.Fishbowl.exceptionThrownBy;
import com.projectpub.ordering.services.impl.OrderingServiceImpl;
import com.projectpub.ordering.dao.OrderingLocalDao;
import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.OrderState;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.extern.log4j.Log4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.*;

/**
 *
 * @author Bob Steers
 */
@Log4j
@RunWith(MockitoJUnitRunner.class)
public class OrderingServiceImplTest {

    @Spy
    private OrderingLocalDao dao;

    @Spy
    private LocalProductServiceImpl productService;

    @InjectMocks
    private OrderingServiceImpl service;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreate() throws Exception {
        ProductOrder order = service.create();
        assertTrue(order.getProducts().isEmpty());
        assertEquals(OrderState.CREATED, order.getOrderState());

        ProductOrder otherOrder = service.create();
        assertNotEquals(order, otherOrder);
        assertNotEquals(order.getId(), otherOrder.getId());
    }

    @Test
    public void testUpdate() throws Exception {
        ProductOrder order = service.create();
        Product product = new Product();
        order.add(product);

        ProductOrder updated = service.update(order);
        assertEquals(order, updated);
    }

    @Test
    public void testFulfill() throws Exception {
        ProductOrder order = service.create();
        exceptionThrownBy(() -> service.fulfill(order), IllegalArgumentException.class);
        
        order.add(new Product());
        ProductOrder updated = service.fulfill(order);

        assertEquals(order.getId(), updated.getId());
        assertEquals(OrderState.MAKEABLE, updated.getOrderState());
    }

    @Test
    public void testTryMake() throws Exception {
        List<ProductOrder> orders = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            ProductOrder o = service.create();
            o.add(new Product());
            orders.add(service.fulfill(o));
        }

        for (int i = 0; i < 10; i++) {
            ProductOrder order = service.tryMake().get();
            assertEquals(orders.get(i), order);
            assertEquals(OrderState.MAKING, order.getOrderState());
        }

        assertFalse(service.tryMake().isPresent());
    }

    private Set<ProductCategory> toCategorySet(ProductCategory... categories) {
        return new HashSet<>(Arrays.asList(categories));
    }

    @Test
    public void testTryMakeSpecific() throws Exception {
        List<Product> products = new ArrayList<>();
        products.add(new Product(1, 1, "food", 0, ProductCategory.FOOD, 5));
        products.add(new Product(2, 2, "food", 0, ProductCategory.FOOD, 5));
        products.add(new Product(3, 3, "drink", 0, ProductCategory.DRINK, 5));
        products.add(new Product(4, 4, "other", 0, ProductCategory.OTHER, 5));

        ProductOrder order = service.create();
        order.setProducts(products);
        service.fulfill(order);

        Optional<ProductOrder> foodOrder = service.tryMake(toCategorySet(ProductCategory.FOOD
        ));

        // only expect food items
        assertEquals(products.subList(0, 2), foodOrder.get().getProducts());
        assertEquals(order.getId(), foodOrder.get().getId());

        // only expect other items
        Optional<ProductOrder> remainingOrder = service.tryMake();
        assertEquals(products.subList(2, 4), remainingOrder.get().getProducts());
        assertNotEquals(order.getId(), remainingOrder.get().getId());
    }

    @Test
    public void testFinishMake() throws Exception {
        ProductOrder order = service.create();
        order.setOrderState(OrderState.MAKING);

        ProductOrder updated = service.finishMake(order);
        assertEquals(order.getId(), updated.getId());
        assertEquals(OrderState.DELIVERABLE, updated.getOrderState());
    }

    @Test(expected = IllegalStateException.class)
    public void testFinishMakeStateNok() throws Exception {
        ProductOrder order = service.create();
        service.finishMake(order);
    }

    @Test
    public void testTryDeliver() throws Exception {
        List<ProductOrder> orders = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            ProductOrder o = service.create();
            o.add(new Product());
            service.fulfill(o);
            orders.add(service.finishMake(service.tryMake().get()));
        }

        for (int i = 0; i < 10; i++) {
            ProductOrder order = service.tryDeliver().get();
            assertEquals(orders.get(i), order);
            assertEquals(OrderState.DELIVERING, order.getOrderState());
        }

        assertFalse(service.tryDeliver().isPresent());
    }

    @Test
    public void testGetDelivered() throws Exception {
        Set<ProductOrder> orders = new HashSet<>();

        for (int i = 0; i < 10; i++) {
            ProductOrder o = service.create();
            o.add(new Product());
            service.fulfill(o);
            orders.add(service.finishMake(service.tryMake().get()));
            orders.add(service.finishDeliver(service.tryDeliver().get()));
        }

        service.getDelivered().stream()
                .peek(order -> assertEquals(OrderState.DELIVERED, order.getOrderState()))
                .peek(order -> assertTrue(orders.contains(order)));
    }

    @Test
    public void testProcessPayment() throws Exception {
        Set<ProductOrder> orders = new HashSet<>();

        for (int i = 0; i < 10; i++) {
            ProductOrder o = service.create();
            o.add(new Product());
            service.fulfill(o);
            orders.add(service.finishMake(service.tryMake().get()));
            orders.add(service.finishDeliver(service.tryDeliver().get()));
        }

        service.getDelivered().stream()
                .forEach(order -> service.processPayment(order));

        assertTrue(service.getDelivered().isEmpty());
    }

}
