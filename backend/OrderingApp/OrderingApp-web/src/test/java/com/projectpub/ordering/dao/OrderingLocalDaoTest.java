/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.dao;

import com.projectpub.ordering.entities.OrderState;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.ProductOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.extern.log4j.Log4j;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@Log4j
@RunWith(MockitoJUnitRunner.class)
public class OrderingLocalDaoTest {

    @InjectMocks
    OrderingLocalDao dao;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private Set<ProductCategory> allCategories() {
        return new HashSet<>(Arrays.asList(ProductCategory.values()));
    }

    @Test
    public void testAddOrder() throws Exception {
        ProductOrder order = new ProductOrder();
        assertEquals(dao.addOrder(order), order);
    }

    @Test
    public void testUpdate() throws Exception {
        ProductOrder order = new ProductOrder(10);
        assertEquals(dao.update(order), order);
    }

    @Test
    public void testFindNextOrder() throws Exception {
        List<ProductOrder> orders = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            ProductOrder order = dao.addOrder(new ProductOrder(i));
            order.setOrderState(OrderState.MAKEABLE);
            orders.add(dao.update(order));
        }

        assertFalse(dao.findNextOrder(
                OrderState.CREATED, OrderState.CREATED, allCategories())
                .isPresent());

        for (int i = 0; i < 10; i++) {
            ProductOrder order = dao.findNextOrder(
                    OrderState.MAKEABLE, OrderState.MAKING, allCategories()).get();
            assertEquals("found order does not match created order",
                    orders.get(i), order);
        }

        assertFalse(dao.findNextOrder(
                OrderState.MAKEABLE, OrderState.MAKING, allCategories())
                .isPresent());
    }

    @Test
    public void testqueuedProductCount() throws Exception {
        Product prod1 = new Product(1, 1, "prod1", 100, ProductCategory.OTHER, 5);
        Product prod2 = new Product(2, 2, "prod2", 100, ProductCategory.FOOD, 5);

        for (int i = 0; i < 10; i++) {
            ProductOrder order = dao.addOrder(new ProductOrder(i));
            order.setOrderState(OrderState.MAKEABLE);
            order.add(prod1);
            order.add(prod1);
            order.add(prod2);
            dao.update(order);
        }

        assertEquals(10 * 2, dao.queuedProductCount(prod1, OrderState.MAKEABLE));
        assertEquals(10 * 1, dao.queuedProductCount(prod2, OrderState.MAKEABLE));
        assertEquals(0, dao.queuedProductCount(prod1, OrderState.CREATED));
    }

    @Test
    public void testFindOrders() throws Exception {
        for (int i = 0; i < 10; i++) {
            ProductOrder delivered = dao.addOrder(new ProductOrder(i));
            delivered.setOrderState(OrderState.DELIVERED);
            dao.update(delivered);

            ProductOrder makeable = dao.addOrder(new ProductOrder(i + 10));
            makeable.setOrderState(OrderState.MAKEABLE);
            dao.update(makeable);
        }

        assertTrue(dao.findOrders(OrderState.CREATED).isEmpty());
        assertTrue(dao.findOrders(OrderState.DELIVERED).stream()
                .allMatch(order -> order.getOrderState() == OrderState.DELIVERED));
        assertTrue(dao.findOrders(OrderState.DELIVERED).stream()
                .allMatch(order -> order.getId() % 2 == 1));
        assertTrue(dao.findOrders(OrderState.MAKEABLE).stream()
                .allMatch(order -> order.getOrderState() == OrderState.MAKEABLE));
        assertTrue(dao.findOrders(OrderState.MAKEABLE).stream()
                .allMatch(order -> order.getId() % 2 == 0));
    }

}
