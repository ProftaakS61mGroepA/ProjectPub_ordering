/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.rest;

import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductCategory;
import com.projectpub.ordering.entities.ProductOrder;
import static java.net.HttpURLConnection.HTTP_OK;
import java.util.List;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Bob Steers
 */
public class APITestUtils {

    static void assertStatus(Response response, int expectedResponseCode) {
        assertEquals(response.getStatusInfo().toString(),
                expectedResponseCode, response.getStatus());
    }

    static ProductOrder readProductOrder(Response response) {
        assertStatus(response, HTTP_OK);
        return response.readEntity(ProductOrder.class);
    }

    static List<ProductOrder> readProductOrderList(Response response) {
        assertStatus(response, HTTP_OK);
        return response.readEntity(new GenericType<List<ProductOrder>>() {
        });
    }

    static List<Product> readProductList(Response response) {
        assertStatus(response, HTTP_OK);
        return response.readEntity(new GenericType<List<Product>>() {
        });
    }

    static List<ProductCategory> readCategoryList(Response response) {
        return response.readEntity(new GenericType<List<ProductCategory>>() {
        });
    }
}
