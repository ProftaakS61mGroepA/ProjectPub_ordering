/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projectpub.ordering.rest;

import com.projectpub.ordering.entities.ProductCategory;
import static com.projectpub.ordering.entities.ProductCategory.OTHER;
import com.projectpub.ordering.entities.OrderState;
import com.projectpub.ordering.entities.Product;
import com.projectpub.ordering.entities.ProductOrder;
import static com.projectpub.ordering.rest.APITestUtils.assertStatus;
import static com.projectpub.ordering.rest.APITestUtils.readProductOrderList;
import com.projectpub.ordering.services.IOrderingService;
import com.projectpub.ordering.services.impl.OrderingServiceImpl;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Bob Steers
 */
@Log4j
@RunWith(MockitoJUnitRunner.class)
public class OrderingAPITest extends JerseyTest {

    @Mock
    private OrderingServiceImpl service;

    @Override
    public Application configure() {
        ResourceConfig config = new ResourceConfig(OrderingAPI.class);
        config.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(service).to(IOrderingService.class);
            }
        });
        return config;
    }

    @Before
    public void setUpTest() throws Exception {

    }

    @After
    public void tearDownTest() throws Exception {

    }

    private ProductOrder readProductOrder(Response response) {
        assertStatus(response, HTTP_OK);
        return response.readEntity(ProductOrder.class);
    }

    private Response requestCreate() {
        return target("/ordering/create")
                .request(MediaType.APPLICATION_JSON)
                .get();
    }

    private Response requestUpdate(ProductOrder order) {
        return target("/ordering/update")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(order));
    }

    private Response requestFulfill(ProductOrder order) {
        return target("/ordering/fulfill")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(order));
    }

    private Response requestTryMake() {
        return target("/ordering/trymake")
                .request(MediaType.APPLICATION_JSON)
                .get();
    }

    private Response requestTryMake(List<ProductCategory> categories) {
        return target("/ordering/categorytrymake")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(categories));
    }

    private Response requestFinishMake(ProductOrder order) {
        return target("/ordering/finishmake")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(order));
    }

    private Response requestTryDeliver() {
        return target("/ordering/trydeliver")
                .request(MediaType.APPLICATION_JSON)
                .get();
    }

    private Response requestFinishDeliver(ProductOrder order) {
        return target("/ordering/finishdeliver")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(order));
    }

    private Response requestQueueCount(Product product) {
        return target("/ordering/queuecount")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.json(product));
    }

    private Response requestGetDelivered() {
        return target("/ordering/alldelivered")
                .request(MediaType.APPLICATION_JSON)
                .get();
    }

    private Response requestProcessPayment(ProductOrder order) {
        return target("/ordering/pay")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(order));
    }

    @Test
    public void testCreateOrder() throws Exception {
        ProductOrder expected = new ProductOrder(100);
        when(service.create()).thenReturn(expected);

        Response response = requestCreate();
        assertStatus(response, HTTP_OK);
        ProductOrder order = readProductOrder(response);

        assertNotEquals(0, order.getId());
        assertTrue(order.getProducts().isEmpty());
        assertEquals(expected, order);
    }

    @Test
    public void testCreateOrderNok() throws Exception {
        when(service.create()).thenThrow(new IllegalArgumentException());

        Response response = requestCreate();
        assertStatus(response, HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testUpdateOrder() throws Exception {
        ProductOrder order = new ProductOrder(100);
        order.add(new Product(1, 1, "testproduct", 200, OTHER, 5));

        when(service.update(any(ProductOrder.class)))
                .then(returnsFirstArg());

        Response response = requestUpdate(order);
        assertStatus(response, HTTP_OK);
        ProductOrder updated = readProductOrder(response);

        assertEquals(order, updated);
    }

    @Test
    public void testUpdateOrderNok() throws Exception {
        when(service.update(any())).thenThrow(new IllegalArgumentException());

        Response response = requestUpdate(new ProductOrder());
        assertStatus(response, HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testFulfillOrder() throws Exception {
        ProductOrder order = new ProductOrder(100);
        order.add(new Product(1, 1, "testproduct", 200, OTHER, 5));

        when(service.fulfill(any(ProductOrder.class)))
                .then(returnsFirstArg());

        Response response = requestFulfill(order);
        assertStatus(response, HTTP_OK);
        ProductOrder updated = readProductOrder(response);

        assertEquals(order, updated);
    }

    @Test
    public void testFulfillOrderNok() throws Exception {
        when(service.fulfill(any())).thenThrow(new IllegalArgumentException());

        Response response = requestFulfill(new ProductOrder());
        assertStatus(response, HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testTryMake() throws Exception {
        ProductOrder order = new ProductOrder(100);
        order.setOrderState(OrderState.MAKING);
        when(service.tryMake()).thenReturn(Optional.of(order));

        Response response = requestTryMake();
        assertStatus(response, HTTP_OK);

        assertEquals(order, readProductOrder(response));
    }

    @Test
    public void testTryMakeNotFound() throws Exception {
        when(service.tryMake()).thenReturn(Optional.ofNullable(null));

        Response response = requestTryMake();
        assertStatus(response, HTTP_NO_CONTENT);
    }

    @Test
    public void testTryMakeCategory() throws Exception {
        ProductOrder order = new ProductOrder(100);
        order.setOrderState(OrderState.MAKING);
        when(service.tryMake(any(Set.class))).thenReturn(Optional.of(order));

        Response response = requestTryMake(Arrays.asList(ProductCategory.FOOD, ProductCategory.OTHER));
        assertStatus(response, HTTP_OK);

        assertEquals(order, readProductOrder(response));
    }

    @Test
    public void testFinishMake() throws Exception {
        ProductOrder order = new ProductOrder(100);

        when(service.finishMake(any(ProductOrder.class))).then(returnsFirstArg());

        Response response = requestFinishMake(order);
        assertStatus(response, HTTP_OK);
        assertEquals(order, readProductOrder(response));
    }

    @Test
    public void testTryDeliver() throws Exception {
        ProductOrder order = new ProductOrder(100);
        order.setOrderState(OrderState.DELIVERING);
        when(service.tryDeliver()).thenReturn(Optional.of(order));

        Response response = requestTryDeliver();
        assertStatus(response, HTTP_OK);

        assertEquals(order, readProductOrder(response));
    }

    @Test
    public void testTryDeliverNotFound() throws Exception {
        when(service.tryDeliver()).thenReturn(Optional.ofNullable(null));

        Response response = requestTryDeliver();
        assertStatus(response, HTTP_NO_CONTENT);
    }

    @Test
    public void testFinishDeliver() throws Exception {
        ProductOrder order = new ProductOrder(100);

        when(service.finishDeliver(any(ProductOrder.class))).then(returnsFirstArg());

        Response response = requestFinishDeliver(order);
        assertStatus(response, HTTP_OK);
        assertEquals(order, readProductOrder(response));
    }

    @Test
    public void testQueueCount() throws Exception {
        Product product = new Product(1, 1, "prod", 100, ProductCategory.OTHER, 5);
        when(service.queuedProductCount(any(Product.class))).thenReturn(new Long(42));

        Response response = requestQueueCount(product);
        assertStatus(response, HTTP_OK);
        // Assert that it really only is sending the number - without any wrapping
        assertEquals("42", response.readEntity(String.class));
    }

    @Test
    public void testGetDelivered() throws Exception {
        ProductOrder order1 = new ProductOrder(100);
        ProductOrder order2 = new ProductOrder(200);

        when(service.getDelivered()).thenReturn(Arrays.asList(order1, order2));

        Response response = requestGetDelivered();
        List<ProductOrder> orders = readProductOrderList(response);
        assertEquals(order1, orders.get(0));
        assertEquals(order2, orders.get(1));
    }

    @Test
    public void testProcessPayment() throws Exception {
        ProductOrder order = new ProductOrder(100);

        Response response = requestProcessPayment(order);
        assertStatus(response, HTTP_NO_CONTENT);

        verify(service, times(1)).processPayment(order);
    }

}
