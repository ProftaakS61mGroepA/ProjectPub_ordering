import { Pipe, PipeTransform } from '@angular/core';
import { ProductOrder } from './services/all.service';

@Pipe({
  name: 'orderprice'
})
export class OrderpricePipe implements PipeTransform {

  transform(order: ProductOrder, args?: any): number {
    let total = 0;
    order.products.forEach(prod => total += prod.priceInCents);
    return total / 100;
  }

}
