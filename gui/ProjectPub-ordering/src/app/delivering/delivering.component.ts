import { Component, NgZone } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { OnInit } from '@angular/core';

import { ProductOrder, OrderingService } from '../services/all.service';

@Component({
  selector: 'app-delivering',
  templateUrl: './delivering.component.html',
  styleUrls: ['./delivering.component.css']
})
export class DeliveringComponent implements OnInit {

  currentOrder: ProductOrder = null;

  constructor(
    private _zone: NgZone,
    private orderingService: OrderingService
  ) { }

  ngOnInit(): void {

  }

  requestOrder(): void {
    if (!this.currentOrder) {
      this.orderingService.tryDeliver().subscribe(order => {
        this._zone.run(() => this.currentOrder = order);
      });
    }
  }

  finishOrder(): void {
    this.orderingService.finishDeliver(this.currentOrder).subscribe(order => {
      this._zone.run(() => this.currentOrder = null);
    });
  }

}
