import { CategoryFormatPipe } from './category-format.pipe';

describe('CategoryFormatPipe', () => {
  it('create an instance', () => {
    const pipe = new CategoryFormatPipe();
    expect(pipe).toBeTruthy();
  });

  it('should capitalise first letter from caps', () => {
    const pipe = new CategoryFormatPipe();
    expect(pipe.transform('CAPITALS')).toBe('Capitals');
    expect(pipe.transform('lowercase')).toBe('Lowercase');
    expect(pipe.transform('i')).toBe('I');
    expect(pipe.transform('')).toBe('');
    expect(pipe.transform(null)).toBe('');
  });

});
