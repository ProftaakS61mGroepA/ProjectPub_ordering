import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categoryFormat'
})
export class CategoryFormatPipe implements PipeTransform {

  transform(value: string, args?: any): string {
    return (value != null && value.length > 0)
      ? value.charAt(0).toUpperCase() + value.slice(1).toLowerCase()
      : '';
  }

}
