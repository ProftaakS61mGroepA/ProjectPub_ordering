import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCardModule, MatMenuModule, MatToolbarModule, MatIconModule, MatTabsModule } from '@angular/material';

import { AppComponent } from './app.component';

import { OrderingService, ProductService, ConfigService, WebsocketService } from './services/all.service';
import { MakingComponent, OrderingComponent, DeliveringComponent, PaymentComponent } from './all.component';
import { CategoryFormatPipe } from './category-format.pipe';
import { OrderpricePipe } from './orderprice.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MakingComponent,
    OrderingComponent,
    DeliveringComponent,
    CategoryFormatPipe,
    PaymentComponent,
    OrderpricePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatTabsModule
  ],
  providers: [
    OrderingService,
    ProductService,
    ConfigService,
    WebsocketService
  ],
  bootstrap: [
    AppComponent
  ],
  exports: [
    CategoryFormatPipe
  ]
})
export class AppModule { }
