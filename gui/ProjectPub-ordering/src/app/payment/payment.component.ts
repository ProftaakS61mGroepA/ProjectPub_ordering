import { Component, OnInit, NgZone } from '@angular/core';

import { ProductOrder, OrderingService, WebsocketService } from '../services/all.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  public orders: ProductOrder[] = [];
  private _connected = false;

  constructor(
    private _zone: NgZone,
    private orderingService: OrderingService,
    private wsService: WebsocketService
  ) { }

  ngOnInit() {
    this.update();
  }

  pay(order: ProductOrder) {
    this.orderingService.processPayment(order).subscribe(response => console.log('order paid'));
  }

  subscribe(reconnect = false) {
    this.wsService.connect(reconnect).subscribe(message => {
      const data = JSON.parse(message.data);
      this.onUpdatePush(data.update);
      this.onRemovePush(data.remove);
    }, error => {
      console.log('websocket connection error: ' + error);
    }, () => {
      console.log('websocket connection completed');
      this.subscribe(true);
    });
  }

  update() {
    this.orderingService.getDelivered().subscribe(
      orders => this._zone.run(() => this.orders = orders ? orders : []));
    this.subscribe();
  }

  onUpdatePush(updated: ProductOrder) {
    if (!updated) {
      return;
    }
    this._zone.run(() => {
      this.orders = this.orders.filter(order => order.id !== updated.id);
      this.orders.push(updated);
    });
  }

  onRemovePush(updated: ProductOrder) {
    if (!updated) {
      return;
    }
    this._zone.run(() => this.orders = this.orders.filter(order => order.id !== updated.id));
  }
}
