import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MockBackend } from '@angular/http/testing';

import { ProductOrder, OrderingService } from './ordering.service';
import { Product } from './product.service';

import { ConfigService } from './config.service';
import { MockConfigService } from './config.service.mock';

describe('OrderingService', () => {
  let httpMock: HttpTestingController;
  let service: OrderingService;
  let configMock: ConfigService;

  // before each in this description
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: ConfigService, useClass: MockConfigService },
        OrderingService
      ]
    }).compileComponents();

    service = TestBed.get(OrderingService);
    httpMock = TestBed.get(HttpTestingController);
    configMock = TestBed.get(ConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should use the config mock', () => {
    expect(configMock.getOrderingApi()).toBe('ordering_api/');
  });

  it('create() should return a ProductOrder', () => {
    service.create().subscribe((order) => {
      expect(order.id).toBe(1);
      expect(order.products).toEqual([]);
    });

    const request = httpMock.expectOne('ordering_api/create');
    request.flush({ id: 1, products: [] });
    httpMock.verify();
  });

  it('update() should return a ProductOrder', () => {
    const order = new ProductOrder();
    order.products.push(new Product(12, 'testey', 100));

    service.update(order).subscribe((updated) => {
      expect(updated.id).toBe(2);
      expect(updated.products).toEqual(order.products);
    });

    const request = httpMock.expectOne('ordering_api/update');
    const response = order;
    response.id = 2;
    request.flush(response);
    httpMock.verify();
  });

  it('fulfill() should return a ProductOrder', () => {
    const order = new ProductOrder();
    order.products.push(new Product(12, 'testey', 100));

    service.fulfill(order).subscribe((updated) => {
      expect(updated.id).toBe(2);
      expect(updated.products).toEqual(order.products);
    });

    const request = httpMock.expectOne('ordering_api/fulfill');
    const response = order;
    response.id = 2;
    request.flush(response);
    httpMock.verify();
  });

  it('tryMake() should return a productOrder', () => {
    const order = new ProductOrder();
    order.products.push(new Product(12, 'testey', 100));

    service.tryMake().subscribe((item) => {
      expect(item).toEqual(order);
    });

    httpMock.expectOne('ordering_api/trymake').flush(order);
    httpMock.verify();
  });

  it('tryMake() can return nothing', () => {
    service.tryMake().subscribe((item) => {
      expect(item).toEqual(null);
    });

    httpMock.expectOne('ordering_api/trymake')
      .flush('', {
        status: 204,
        statusText: 'no product order found'
      });
    httpMock.verify();
  });

  it('finishMake() should return a productOrder', () => {
    const order = new ProductOrder();
    order.products.push(new Product(12, 'testey', 100));

    service.finishMake(order).subscribe((item) => {
      expect(item).toEqual(order);
    });

    httpMock.expectOne('ordering_api/finishmake').flush(order);
    httpMock.verify();
  });

  it('tryDeliver() should return a productOrder', () => {
    const order = new ProductOrder();
    order.products.push(new Product(12, 'testey', 100));

    service.tryDeliver().subscribe((item) => {
      expect(item).toEqual(order);
    });

    httpMock.expectOne('ordering_api/trydeliver').flush(order);
    httpMock.verify();
  });

  it('tryDeliver() can return nothing', () => {
    service.tryDeliver().subscribe((item) => {
      expect(item).toEqual(null);
    });

    httpMock.expectOne('ordering_api/trydeliver')
      .flush('', {
        status: 204,
        statusText: 'no product order found'
      });
    httpMock.verify();
  });

  it('finishDeliver() should return a productOrder', () => {
    const order = new ProductOrder();
    order.products.push(new Product(12, 'testey', 100));

    service.finishDeliver(order).subscribe((item) => {
      expect(item).toEqual(order);
    });

    httpMock.expectOne('ordering_api/finishdeliver').flush(order);
    httpMock.verify();
  });

});
