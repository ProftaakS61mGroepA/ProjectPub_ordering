import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from './config.service';

export class Product {
  get priceEuro(): number {
    console.log(this.name + ' = ' + this.priceInCents);
    return this.priceInCents / 100;
  }

  constructor(
    public id: number,
    public name: string,
    public priceInCents: number,
    public queueCount: number = 0,
    public available: number = 0
  ) { }

}

@Injectable()
export class ProductService {
  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) { }

  public getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.config.getProductApi() + 'all');
  }

  public getCategories(): Observable<string[]> {
    return this.http.get<string[]>(this.config.getProductApi() + 'categories');
  }

}
