import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from './config.service';
import { Product } from './product.service';

export class ProductOrder {
  public id: number;
  public products: Product[] = [];
}

@Injectable()
export class OrderingService {
  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) { }

  private api(endpoint: string) {
    return this.config.getOrderingApi() + endpoint;
  }

  public create(): Observable<ProductOrder> {
    return this.http.get<ProductOrder>(this.api('create'));
  }

  public update(order: ProductOrder): Observable<ProductOrder> {
    return this.http.put<ProductOrder>(this.api('update'), order);
  }

  public fulfill(order: ProductOrder): Observable<ProductOrder> {
    return this.http.put<ProductOrder>(this.api('fulfill'), order);
  }

  public tryMake(): Observable<ProductOrder> {
    return this.http.get<ProductOrder>(this.api('trymake'));
  }

  public tryMakeSpecific(categories: string[]): Observable<ProductOrder> {
    return this.http.put<ProductOrder>(this.api('categorytrymake'), categories);
  }

  public finishMake(order: ProductOrder): Observable<ProductOrder> {
    return this.http.put<ProductOrder>(this.api('finishmake'), order);
  }

  public tryDeliver(): Observable<ProductOrder> {
    return this.http.get<ProductOrder>(this.api('trydeliver'));
  }

  public finishDeliver(order: ProductOrder): Observable<ProductOrder> {
    return this.http.put<ProductOrder>(this.api('finishdeliver'), order);
  }

  public queuedProductCount(product: Product): Observable<number> {
    return this.http.post<number>(this.api('queuecount'), product);
  }

  public getDelivered(): Observable<ProductOrder[]> {
    return this.http.get<ProductOrder[]>(this.api('alldelivered'));
  }

  public processPayment(order: ProductOrder): Observable<void> {
    return this.http.put<void>(this.api('pay'), order);
  }
}
