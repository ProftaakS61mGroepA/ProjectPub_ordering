import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';

@Injectable()
export class MockConfigService extends ConfigService {

    getOrderingApi(): string {
        return 'ordering_api/';
    }

    getProductApi(): string {
        return 'product_api/';
    }
}
