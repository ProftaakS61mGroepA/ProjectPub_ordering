import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MockBackend } from '@angular/http/testing';

import { Product, ProductService } from './product.service';
import { ConfigService } from './config.service';
import { MockConfigService } from './config.service.mock';

describe('ProductService', () => {
  let service: ProductService;
  let httpMock: HttpTestingController;
  let configMock: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ProductService,
        { provide: ConfigService, useClass: MockConfigService }
      ]
    }).compileComponents();

    service = TestBed.get(ProductService);
    httpMock = TestBed.get(HttpTestingController);
    configMock = TestBed.get(ConfigService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should use config mock', () => {
    expect(configMock.getProductApi()).toBe('product_api/');
  });

  it('getAll() should return expected products', () => {
    const expectedProducts = [
      new Product(1, 'first', 1.0),
      new Product(2, 'second', 2.0)
    ];

    service.getAll().subscribe((products) => {
      expect(products).toBe(expectedProducts);
    });

    const request = httpMock.expectOne('product_api/all');
    request.flush(expectedProducts);
    httpMock.verify();
  });

  it('getCategories() should return expected categories', () => {
    const expectedCategories = ['cat1', 'cat2'];

    service.getCategories().subscribe((cats) => {
      expect(cats).toBe(expectedCategories);
    });

    const request = httpMock.expectOne('product_api/categories');
    request.flush(expectedCategories);
    httpMock.verify();
  });
});
