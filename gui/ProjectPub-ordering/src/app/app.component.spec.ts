import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCardModule, MatMenuModule, MatToolbarModule, MatIconModule, MatTabsModule} from '@angular/material';

import { OrderingService, ProductService, ConfigService, WebsocketService } from './services/all.service';

import { CategoryFormatPipe } from './category-format.pipe';
import { OrderpricePipe } from './orderprice.pipe';

import { OrderingComponent, MakingComponent, DeliveringComponent, PaymentComponent } from './all.component';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatMenuModule,
        MatCardModule,
        MatTabsModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        OrderingComponent,
        MakingComponent,
        DeliveringComponent,
        PaymentComponent,
        CategoryFormatPipe,
        OrderpricePipe
      ],
      providers: [
        OrderingService,
        ProductService,
        ConfigService,
        WebsocketService
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  // Commented as example code when GUI elements are implemented

  // it(`should have as title 'app'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('app');
  // }));

  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  // }));
});
