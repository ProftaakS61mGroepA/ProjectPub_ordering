import { TestBed, async, ComponentFixture} from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCardModule, MatMenuModule, MatToolbarModule, MatIconModule } from '@angular/material';

import { OrderingService, ProductService, ConfigService } from '../services/all.service';

import { CategoryFormatPipe } from '../category-format.pipe';

import { MakingComponent } from './making.component';

describe('MakingComponent', () => {
  let component: MakingComponent;
  let fixture: ComponentFixture<MakingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatMenuModule,
        MatCardModule,
        FormsModule
      ],
      declarations: [
        MakingComponent,
        CategoryFormatPipe
      ],
      providers: [
        OrderingService,
        ProductService,
        ConfigService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
