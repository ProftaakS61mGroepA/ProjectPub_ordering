import { Component, NgZone } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { OnInit } from '@angular/core';

import { ProductOrder, OrderingService, Product, ProductService } from '../services/all.service';

import { CategoryFormatPipe } from '../category-format.pipe';

class CheckedCategory {
  constructor(
    public category: string,
    public checked: boolean = true
  ) { }
}

@Component({
  selector: 'app-making',
  templateUrl: './making.component.html',
  styleUrls: ['./making.component.css']
})
export class MakingComponent implements OnInit {

  private _currentOrder: ProductOrder = null;
  categories: CheckedCategory[] = [];

  get currentOrder() { return this._currentOrder; }
  set currentOrder(order: ProductOrder) {
    if (order) {
      this.addQueueCounts(order);
    }
    this._currentOrder = order;
  }

  constructor(
    private _zone: NgZone,
    private orderingService: OrderingService,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.productService.getCategories().subscribe(cats => {
      this._zone.run(() => this.categories = cats.map(cat => new CheckedCategory(cat)));
    });
  }

  requestOrder(): void {
    if (!this.currentOrder) {
      this.orderingService
        .tryMakeSpecific(this.getSelectedCategories(this.categories))
        .subscribe(order => this._zone.run(() => this.currentOrder = order));
    }
  }

  finishOrder(): void {
    this.orderingService.finishMake(this.currentOrder).subscribe(order => {
      this._zone.run(() => this.currentOrder = null);
    });
  }

  getSelectedCategories(selectedCats: CheckedCategory[]): string[] {
    return selectedCats
      .filter(cat => cat.checked)
      .map(cat => cat.category);
  }

  addQueueCounts(order: ProductOrder) {
      order.products.forEach(prod => {
        this.orderingService.queuedProductCount(prod).subscribe(
          count => this._zone.run(() => prod.queueCount = count));
      });
  }

}
