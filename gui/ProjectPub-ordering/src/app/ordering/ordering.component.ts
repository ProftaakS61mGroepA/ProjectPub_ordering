import { Component, NgZone } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { OnInit } from '@angular/core';

import { Product, ProductService, ProductOrder, OrderingService } from '../services/all.service';

@Component({
  selector: 'app-ordering',
  templateUrl: './ordering.component.html',
  styleUrls: ['./ordering.component.css']
})
export class OrderingComponent implements OnInit {

  products: Product[] = [];
  currentOrder: ProductOrder;
  orderCounts: Number[];

  constructor(
    private _zone: NgZone,
    private ProductService: ProductService,
    private orderingService: OrderingService
  ) { }

  ngOnInit(): void {
    this.ProductService.getAll().subscribe(data => {
      this._zone.run(() => this.products = data);
    });
  }

  createOrder() {
    this.orderingService.create().subscribe(order => {
      this._zone.run(() => this.currentOrder = order);
    });
  }

  updateOrder(product) {
    this.currentOrder.products.push(product);
    this.orderingService.update(this.currentOrder).subscribe(order => {
      this._zone.run(() => this.currentOrder = order);
    });
  }

  fulfillOrder() {
    this.orderingService.fulfill(this.currentOrder).subscribe(order => {
      this._zone.run(() => this.currentOrder = null);
    });
  }

}
