export const environment = {
  production: true,
  ordering_api: ':10011/ordering/',
  product_api: ':10011/products/',
  notification_endpoint: ':10011/delivered'
};
