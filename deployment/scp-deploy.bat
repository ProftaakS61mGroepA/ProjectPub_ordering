set target=192.168.27.128
putty -ssh -pw student student@%target% -m %~dp0/ssh-commands.txt
pscp -r -pw student %~dp0/../gui/ProjectPub-ordering/dist/* student@%target%:git/ProjectPub/download/order-frontend-build/
pscp -r -pw student %~dp0/../backend/OrderingApp/OrderingApp-web/target/*.war student@%target%:git/ProjectPub/download/order-backend-build/